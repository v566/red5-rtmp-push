package com.ifast.face.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@TableName("USER_FACE_GROUP")
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class UserFaceGroup { 
	@TableId(type= IdType.AUTO)
    private Long id; 
    private String name; 
    @Override
    public String toString(){
        return name;
    } 
}
package com.ifast.face.service.impl;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.ifast.face.domain.Camera;
import com.ifast.face.service.CameraService;
import com.ifast.face.service.FaceEngineService;
import com.ifast.face.service.RtmpPushService;
import com.ifast.face.service.run.RtmpTurnRun;
import com.xiaoleilu.hutool.util.CollectionUtil;

@Service
public class RtmpPushServiceImpl implements RtmpPushService{
	
	@Value("#{'${rtmp.url}'.split('!')}")
	private List<String> url; // 接收prop1里面的属性值 
	@Value("${red5.url}")
	private String red5Url;
	@Autowired
	private FaceEngineService faceEngineService;
	private String groupName = "1";
	public static ExecutorService executor = Executors.newCachedThreadPool();
	@Autowired
	private CameraService cameraService; 
	    
	@Override
	public void push(){
		System.out.println(url);
		System.out.println(red5Url);
		if(CollectionUtil.isEmpty(url)) return;
		for(String fetch : url){
			String[] rtmp = fetch.split(","); 
			executor.execute(new RtmpTurnRun(faceEngineService,StringUtils.trimAllWhitespace(rtmp[1]),red5Url+"?token=100/"+StringUtils.trimAllWhitespace(rtmp[0]),groupName));
		}
	}

	@Override
	public void reset() {
		cameraService.update(new UpdateWrapper<Camera>().setSql("status=0,rtmp_url=\"\"").isNotNull("ID"));
	}
}

package com.ifast.face.service.run;

import org.bytedeco.javacpp.avcodec;
import org.bytedeco.javacv.Frame;
import org.bytedeco.javacv.FrameRecorder;
/**
 * 
 * @author shíQíang㊚
 *	2019年8月19日10:25:13
 */
public class FrameRecorderAdapter{
	
	private FrameRecorder recorder;
	
	private int FRAME_RATE = 50; //25的一倍
	private int GOP_LENGTH_IN_FRAMES= 60; 
 
	private String url;
	private int width;
	private int height;
	
	public FrameRecorderAdapter(String url,int width,int height,int frameRate,int gopLengthInFrames){
		this.url = url; 
		this.width = width;
		this.height = height;
		this.FRAME_RATE = frameRate;
		this.GOP_LENGTH_IN_FRAMES = gopLengthInFrames;
	}
	
	
	
	public void record(Frame frame){
		try {
			recorder.record(frame);
		} catch (Exception e) {
			//需要根据 一次类型进行重启restart();
			e.printStackTrace();  
		}
	}
	public void setTimestamp(long timestamp) {
		recorder.setTimestamp(timestamp);
	}
	
	public void start(){ 
		try {
			builder(); 
			recorder.start();
		} catch (Exception e) {
			 e.printStackTrace();
		}//开启录制器
	}
	
	private void builder()throws Exception{ 
		System.out.println("推送url:"+url);
		recorder = FrameRecorder.createDefault(url,width,height);//new FFmpegFrameRecorder(url,width,height,0);
		recorder.setInterleaved(true); 
		/* decrease "startup" latency in FFMPEG (see:
		 https://trac.ffmpeg.org/wiki/Encode/H.264*/ 
		recorder.setVideoOption("tune","zerolatency");
		/* tradeoff between quality and encode speed
		 possible values are ultrafast,superfast, veryfast, faster, fast,
		 medium, slow, slower, veryslow
		 ultrafast offers us the least amount of compression (lower encoder
		 CPU) at the cost of a larger stream size
		 at the other end, veryslow provides the best compression (high
		 encoder CPU) while lowering the stream size
		 (see: https://trac.ffmpeg.org/wiki/Encode/H.264)*/
		recorder.setVideoOption("preset","ultrafast"); 
		//recorder.setVideoOption("thread_queue_size","6");
		//recorder.setVideoOption("acodec","aac");
		// Constant Rate Factor (see: https://trac.ffmpeg.org/wiki/Encode/H.264)
		recorder.setVideoOption("crf","28");
		// 2000 kb/s, reasonable "sane" area for 720
		//recorder.setVideoBitrate(2000000);
		recorder.setVideoCodec(avcodec.AV_CODEC_ID_H264);
		recorder.setFormat("flv");
		// FPS (frames per second)
		recorder.setFrameRate(FRAME_RATE);
		// Key frame interval, in our case every 2 seconds -> 30 (fps) * 2 = 60
		// (gop length)
		recorder.setGopSize(GOP_LENGTH_IN_FRAMES); 
		  
//		// We don't want variable bitrate audio
//		recorder.setAudioOption("crf","0");
//		// Highest quality
//		recorder.setAudioQuality(0);
//		// 192 Kbps
//		recorder.setAudioBitrate(192000);
//		recorder.setSampleRate(44100);
 		//recorder.setAudioChannels(0);
//		recorder.setAudioCodec(avcodec.AV_CODEC_ID_AAC);
	} 
	
	public void close() throws org.bytedeco.javacv.FrameRecorder.Exception{
		recorder.stop(); 
		recorder.release();
	} 
}

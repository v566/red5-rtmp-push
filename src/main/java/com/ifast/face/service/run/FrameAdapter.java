package com.ifast.face.service.run;

import org.bytedeco.javacpp.opencv_core.IplImage;
import org.bytedeco.javacv.OpenCVFrameConverter;
/**
 * 封装视频处理的相关参数
 * @author shíQíang㊚
 *
 */
public class FrameAdapter {
	 
 	int captureWidth = 1024;
 	int captureHeight = 576;
 	private final int TIMEOUT = 10; // In seconds.
 	final private static int FRAME_RATE = 25; //25的一倍
	final private static int GOP_LENGTH_IN_FRAMES= 25;
	
	public FrameGrabberAdapter from(String src){
		FrameGrabberAdapter grabber = new FrameGrabberAdapter(src,TIMEOUT,captureWidth,captureHeight,FRAME_RATE);
		grabber.start();//开启抓取器
		OpenCVFrameConverter.ToIplImage converter = new OpenCVFrameConverter.ToIplImage();//转换器
		IplImage grabbedImage = converter.convert(grabber.grab());//抓取一帧视频并将其转换为图像，至于用这个图像用来做什么？加水印，人脸识别等等自行添加
		if(grabbedImage != null){
			captureWidth = grabbedImage.width();
			captureHeight = grabbedImage.height(); 
		} 
		return grabber;
	}
	
	public FrameRecorderAdapter to(String out){
		FrameRecorderAdapter recorder = new FrameRecorderAdapter(out,captureWidth,captureHeight,FRAME_RATE,GOP_LENGTH_IN_FRAMES);
		recorder.start();//开启录制器
		return recorder;
	}
}

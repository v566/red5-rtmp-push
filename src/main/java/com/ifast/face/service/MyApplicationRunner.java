package com.ifast.face.service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

/**
 * shíQíang㊚
 * 2019年8月13日10:04:49
 */
@Component
@Slf4j
public class MyApplicationRunner implements ApplicationRunner {
	@Autowired
	private RtmpPushService rtmpPushService;
	
    @Override
    public void run(ApplicationArguments var1) throws Exception{
        log.debug("========初始化视频推送数据========");
        rtmpPushService.reset();
    }
}
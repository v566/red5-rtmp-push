//package com.ifast.face.controller;
//
//import java.awt.Canvas;
//import java.awt.event.MouseEvent;
//import java.awt.event.MouseListener;
//import java.awt.event.WindowAdapter;
//import java.awt.event.WindowEvent;
//
//import javax.swing.JFrame;
//
//import org.bytedeco.javacv.CanvasFrame;
//import org.bytedeco.javacv.FFmpegFrameGrabber;
//import org.bytedeco.javacv.Frame;
//
//
//public class ShowCamera {
//
//	static CanvasFrame canvas = new CanvasFrame("摄像头");// 新建一个窗口 
//	static FFmpegFrameGrabber fGrabber;
//
//	public static boolean close = true;
//	
//	public static void  init(FFmpegFrameGrabber grabber){
//		
//		fGrabber = grabber; 
//		//监听关闭事件
//		canvas.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
//		canvas.addWindowListener(new WindowAdapter(){ 
//			@Override
//			public void windowClosed(WindowEvent e) {
//				System.out.println("程序退出");
//				try {
//					close = false; 
//					
//					fGrabber.stop();// 停止抓取 
//					System.exit(0);// 退出 
//				} catch (Exception e1) { 
//					e1.printStackTrace();
//				}
//				
//			} 
//		});
//		//canvas.setAlwaysOnTop(true); 
//        // 对canvas设置鼠标监听事件
//		Canvas can = canvas.getCanvas();
//		
//		can.addMouseListener(new MouseListener() {
//
//            @Override
//            public void mouseReleased(MouseEvent e) {
//            }
//
//            @Override
//            public void mousePressed(MouseEvent e) {
//            }
//
//            @Override
//            public void mouseExited(MouseEvent e) {
//            }
//
//            @Override
//            public void mouseEntered(MouseEvent e) {
//            }
//
//            @Override
//            public void mouseClicked(MouseEvent e) {
//                // 控制台输出点击的坐标
//                System.out.println("x: " + e.getX());
//                System.out.println("y: " + e.getY());
//            }
//        }); 
//	} 
//	
//	public static void show(Frame frame){
//		canvas.showImage(frame);// 获取摄像头图像并放到窗口上显示， 
//	} 
//}

package com.ifast.face.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class FaceUserInfo { 
	private Long id;
    private String faceId;
    private String name;
    private Integer similarValue;
    private byte[] faceFeature; 
}

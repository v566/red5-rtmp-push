package com.ifast.common.controller;

import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.ifast.api.util.DataResult;
import com.ifast.common.annotation.Log;
import com.ifast.common.annotation.OperateRecord;
import com.ifast.common.base.AdminBaseController;
import com.ifast.common.domain.Update;
import com.ifast.common.service.UpdateService;
import com.ifast.common.utils.Result;
import com.ifast.sys.filter.SameUrlData;

/**
 * <pre>
 *  APP更新
 * </pre>
 * 
 * <small> 2019年4月24日10:46:13 | ShiQiang </small>
 */
@RequestMapping("/common/app")
@Controller
public class AppUpdateController extends AdminBaseController {
    
	@Autowired
	UpdateService updateService;
	
    String prefix = "common/app/";
    
    @Log("列表页面")
    @GetMapping()
    String log() {
        return prefix + "app";
    }
    
    @Log("查询列表")
    @ResponseBody
    @GetMapping("/list")
    public Result<IPage<Update>> list(Update logDTO) {
        // 查询列表数据
        IPage<Update> page = updateService.page(getPage(Update.class));
        return Result.ok(page);
    }
    
    
    @GetMapping("/add") 
    public String addText(Model model, String id){
    	return  prefix+"add";
    }
   
    @GetMapping("/edit/{id}") 
    public String editText(Model model,@PathVariable("id") String id){
    	model.addAttribute("info",updateService.getOne(new QueryWrapper<Update>().eq("type", id)));
        return  prefix+"edit";
    } 
    
    
    @Log("删除")
    @ResponseBody
    @PostMapping("/remove")
    Result<String> remove(Long id) {
    	updateService.removeById(id);
        return Result.ok();
    }
    
    @Log("批量删除")
    @ResponseBody
    @PostMapping("/batchRemove")
    Result<String> batchRemove(@RequestParam("ids[]") Long[] ids) {
    	updateService.removeByIds(Arrays.asList(ids));
        return Result.fail();
    }
    
    /**
     * 保存 
     */
    @PostMapping("/save")
    @ResponseBody
    @SameUrlData
    @OperateRecord(OperateName = "保存",ModellName ="apk更新")
    public Result<?> save(HttpServletRequest request,Update news) {
        try{  
        	DataResult<?> result = updateService.add(request,news);
        	return DataResult.CODE_SUCCESS.equals(result.getCode()) ? Result.ok():Result.fail(result.getMsg());
        }catch (Exception e){
            log.error(e.toString(),e);
            return Result.fail();
        } 
    } 
}

/**
 * 
 */
package com.ifast.common.service.impl;


import com.ifast.common.service.RedisParentManager;
import com.ifast.common.utils.GjpLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.SetOperations;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/** 
 * 项目名称：hongshan_app
 * 类名称：RedisParentService
 * 类描述： 
 * 创建人：Administrator 
 * 创建时间：2017年5月18日 下午3:06:23
 * 修改人：Administrator 
 * 修改时间：2017年5月18日 下午3:06:23
 * 修改备注： 
 * @version 
 */
@Service
public class RedisParentService implements RedisParentManager {
	
	@Autowired
    public RedisTemplate<String, String> redisTemplateKeyString;
    /**
     * 前缀
     */
    public static final String KEY_PREFIX_VALUE = "dg:value:";
    public static final String KEY_PREFIX_SET = "dg:set:";
    public static final String KEY_PREFIX_LIST = "dg:list:";

    /**
     * 缓存value操作
     * @param k
     * @param v
     * @param time
     * @return
     */
    @Override
    public boolean cacheValue(String k, String v, long time) {
        String key = KEY_PREFIX_VALUE + k;
        try {
            ValueOperations<String, String> valueOps =  redisTemplateKeyString.opsForValue();
            valueOps.set(key, v);
            if (time > 0) {
                redisTemplateKeyString.expire(key, time, TimeUnit.SECONDS);
            }
            return true;
        } catch (Throwable t) {
            GjpLogger.error("缓存[" + key + "]失败, value[" + v + "]", t);
        }
        return false;
    }

    /**
     * 缓存value操作
     * @param k
     * @param v
     * @param time
     * @return
     */
    public boolean cacheValueForMill(String k, String v, long time) {
        String key = KEY_PREFIX_VALUE + k;
        try {
            ValueOperations<String, String> valueOps =  redisTemplateKeyString.opsForValue();
            valueOps.set(key, v);
            if (time > 0) {
                redisTemplateKeyString.expire(key, time, TimeUnit.MILLISECONDS);
            }
            return true;
        } catch (Throwable t) {
            GjpLogger.error("缓存[" + key + "]失败, value[" + v + "]", t);
        }
        return false;
    }

    /**
     * 缓存value操作
     * @param k
     * @param v
     * @return
     */
    @Override
    public boolean cacheValue(String k, String v) {
        return cacheValue(k, v, -1);
    }

    /**
     * 判断缓存是否存在
     * @param k
     * @return
     */
    @Override
    public boolean containsValueKey(String k) {
        return containsKey(KEY_PREFIX_VALUE + k);
    }

    /**
     * 判断缓存是否存在
     * @param k
     * @return
     */
    @Override
    public boolean containsSetKey(String k) {
        return containsKey(KEY_PREFIX_SET + k);
    }

    /**
     * 判断缓存是否存在
     * @param k
     * @return
     */
    @Override
    public boolean containsListKey(String k) {
        return containsKey(KEY_PREFIX_LIST + k);
    }

    @Override
    public boolean containsKey(String key) {
        try {
            return redisTemplateKeyString.hasKey(key);
        } catch (Throwable t) {
            GjpLogger.error("判断缓存存在失败key[" + key + ", error[" + t + "]");
        }
        return false;
    }

    /**
     * 获取缓存
     * @param k
     * @return
     */
    @Override
    public String getValue(String k) {
        try {
            ValueOperations<String, String> valueOps =  redisTemplateKeyString.opsForValue();
            return valueOps.get(KEY_PREFIX_VALUE + k);
        } catch (Throwable t) {
            GjpLogger.error("获取缓存失败key[" + KEY_PREFIX_VALUE + k + ", error[" + t + "]");
        }
        return null;
    }

    /**
     * 移除缓存
     * @param k
     * @return
     */
    @Override
    public boolean removeValue(String k) {
        return remove(KEY_PREFIX_VALUE + k);
    }

    @Override
    public boolean removeSet(String k) {
        return remove(KEY_PREFIX_SET + k);
    }

    @Override
    public boolean removeList(String k) {
        return remove(KEY_PREFIX_LIST + k);
    }

    /**
     * 移除缓存
     * @param key
     * @return
     */
    @Override
    public boolean remove(String key) {
        try {
            redisTemplateKeyString.delete(key);
            return true;
        } catch (Throwable t) {
            GjpLogger.error("获取缓存失败key[" + key + ", error[" + t + "]");
        }
        return false;
    }
    /**
     * 缓存set操作
     * @param k
     * @param v
     * @param time
     * @return
     */
    @Override
    public boolean cacheSet(String k, String v, long time) {
        String key = KEY_PREFIX_SET + k;
        try {
            SetOperations<String, String> valueOps =  redisTemplateKeyString.opsForSet();
            valueOps.add(key, v);
            if (time > 0) {
                redisTemplateKeyString.expire(key, time, TimeUnit.SECONDS);
            }
            return true;
        } catch (Throwable t) {
            GjpLogger.error("缓存[" + key + "]失败, value[" + v + "]", t);
        }
        return false;
    }

    /**
     * 缓存set
     * @param k
     * @param v
     * @return
     */
    @Override
    public boolean cacheSet(String k, String v) {
        return cacheSet(k, v, -1);
    }

    /**
     * 缓存set
     * @param k
     * @param v
     * @param time
     * @return
     */
    @Override
    public boolean cacheSet(String k, Set<String> v, long time) {
        String key = KEY_PREFIX_SET + k;
        try {
            SetOperations<String, String> setOps =  redisTemplateKeyString.opsForSet();
            setOps.add(key, v.toArray(new String[v.size()]));
            if (time > 0) {
                redisTemplateKeyString.expire(key, time, TimeUnit.SECONDS);
            }
            return true;
        } catch (Throwable t) {
            GjpLogger.error("缓存[" + key + "]失败, value[" + v + "]", t);
        }
        return false;
    }

    /**
     * 缓存set
     * @param k
     * @param v
     * @return
     */
    @Override
    public boolean cacheSet(String k, Set<String> v) {
        return cacheSet(k, v, -1);
    }

    /**
     * 获取缓存set数据
     * @param k
     * @return
     */
    @Override
    public Set<String> getSet(String k) {
        try {
            SetOperations<String, String> setOps = redisTemplateKeyString.opsForSet();
            return setOps.members(KEY_PREFIX_SET + k);
        } catch (Throwable t) {
            GjpLogger.error("获取set缓存失败key[" + KEY_PREFIX_SET + k + ", error[" + t + "]");
        }
        return null;
    }

    /**
     * list缓存
     * @param k
     * @param v
     * @param time
     * @return
     */
    @Override
    public boolean cacheList(String k, String v, long time) {
        String key = KEY_PREFIX_LIST + k;
        try {
            ListOperations<String, String> listOps =  redisTemplateKeyString.opsForList();
            listOps.rightPush(key, v);
            if (time > 0) {
                redisTemplateKeyString.expire(key, time, TimeUnit.SECONDS);
            }
            return true;
        } catch (Throwable t) {
            GjpLogger.error("缓存[" + key + "]失败, value[" + v + "]", t);
        }
        return false;
    }

    /**
     * 缓存list
     * @param k
     * @param v
     * @return
     */
    @Override
    public boolean cacheList(String k, String v) {
        return cacheList(k, v, -1);
    }

    /**
     * 缓存list
     * @param k
     * @param v
     * @param time
     * @return
     */
    @Override
    public boolean cacheList(String k, List<String> v, long time) {
        String key = KEY_PREFIX_LIST + k;
        try {
            ListOperations<String, String> listOps =  redisTemplateKeyString.opsForList();
            listOps.rightPushAll(key, v);
            if (time > 0) {
                redisTemplateKeyString.expire(key, time, TimeUnit.SECONDS);
            }
            return true;
        } catch (Throwable t) {
            GjpLogger.error("缓存[" + key + "]失败, value[" + v + "]", t);
        }
        return false;
    }

    /**
     * 缓存list
     * @param k
     * @param v
     * @return
     */
    @Override
    public boolean cacheList(String k, List<String> v) {
       return cacheList(k, v, -1);
    }

    /**
     * 获取list缓存
     * @param k
     * @param start
     * @param end
     * @return
     */
    @Override
    public List<String> getList(String k, long start, long end) {
        try {
            ListOperations<String, String> listOps =  redisTemplateKeyString.opsForList();
            return listOps.range(KEY_PREFIX_LIST + k, start, end);
        } catch (Throwable t) {
            GjpLogger.error("获取list缓存失败key[" + KEY_PREFIX_LIST + k + ", error[" + t + "]");
        }
        return null;
    }

    /**
     * 获取总条数, 可用于分页
     * @param k
     * @return
     */
    @Override
    public long getListSize(String k) {
        try {
            ListOperations<String, String> listOps =  redisTemplateKeyString.opsForList();
            return listOps.size(KEY_PREFIX_LIST + k);
        } catch (Throwable t) {
            GjpLogger.error("获取list长度失败key[" + KEY_PREFIX_LIST + k + "], error[" + t + "]");
        }
        return 0;
    }

    /**
     * 获取总条数, 可用于分页
     * @param listOps
     * @param k
     * @return
     */
    @Override
    public long getListSize(ListOperations<String, String> listOps, String k) {
        try {
            return listOps.size(KEY_PREFIX_LIST + k);
        } catch (Throwable t) {
            GjpLogger.error("获取list长度失败key[" + KEY_PREFIX_LIST + k + "], error[" + t + "]");
        }
        return 0;
    }

    /**
     * 移除list缓存
     * @param k
     * @return
     */
    @Override
    public boolean removeOneOfList(String k) {
        try {
            ListOperations<String, String> listOps =  redisTemplateKeyString.opsForList();
            listOps.rightPop(KEY_PREFIX_LIST + k);
            return true;
        } catch (Throwable t) {
            GjpLogger.error("移除list缓存失败key[" + KEY_PREFIX_LIST + k + ", error[" + t + "]");
        }
        return false;
    }

    /**
     * @Author : luzhiqiang
     * @Des   key在redis是null 则存储，有则提示
     * @Param : [key]
     * @Return : boolean
     * @Date : 下午 4:38 2017/9/11 0011
     **/
    @Override
    public boolean setHashKeyForValue(String key){
    	boolean flag = false;
    	if(!this.containsValueKey(key)){
    		this.cacheValueForMill(key, "1", 700L);
    		flag = true;
    	}
    	return flag;
    }

    /**
     * @Author : luzhiqiang
     * @Des 自增
     * @Param : [key, num]
     * @Return : java.lang.Long
     * @Date : 下午 2:51 2017/10/24 0024
     **/
    @Override
    public Long setIncreKey(String key,Integer num){
       return  redisTemplateKeyString.opsForValue().increment(key, 1);
    }
}

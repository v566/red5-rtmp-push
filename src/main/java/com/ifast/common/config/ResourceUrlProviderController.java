package com.ifast.common.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.servlet.resource.ResourceUrlProvider;

/**
 * @author :luzq
 * @date:2018/8/10 0010 上午 10:22
 * @copyright: @2018
 */
@ControllerAdvice
public class ResourceUrlProviderController {


    @Autowired
    private ResourceUrlProvider resourceUrlProvider;

    /**
     *  ModelAttribute 相当于 model.setAttribute("urls",resourceUrlProvider);
     * 前台拿到resourceUrlProvider 可以使用这个对象中方法
     * @return
     */
    /**
     * 此处相当于给所有过controller的请求都返回一个urls的对象,对象指的是ResourceUrlProvider实体类new
     * 相当于 new modelView("urls",this.resourceUrlProvider);
     * @return
     */
    @ModelAttribute("urls")
    public ResourceUrlProvider urls() {
        return this.resourceUrlProvider;
    }
}

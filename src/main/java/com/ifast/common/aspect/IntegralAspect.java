package com.ifast.common.aspect;

import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

/**
 * <pre>
 *  积分切面
 * </pre>
 * <small> 2018年3月22日 | ShiQiang</small>
 */
@Aspect
@Component
public class IntegralAspect {
	
//	@Autowired
//    private IntegralFromService integralFromService;
//   
	/**
	 * 登录积分
	 */ 
//    @After(value="execution(* com.ifast.sys.service.UserService.getUserInfoForApp(..))")
//    public void loginScore() throws Exception {  
//        UserDO currUser = ShiroUtils.getSysUser();
//        if(null != currUser) {
////        	integralFromService.obtainAspectIntegral(currUser.getId().toString(), currUser.getTopDeptId(), Const.integral.integral_login);
//        } 
//    }
//    
//    /**
//     * 照片上传获取积分
//     */ 
//    @After(value="execution(* com.ifast.album.service.AlbumPhotoService.upload(..))")
//    public void uploadScore() throws Throwable {  
//        UserDO currUser = ShiroUtils.getSysUser();
//        if(null != currUser) {
////        	integralFromService.obtainAspectIntegral(currUser.getId().toString(), currUser.getTopDeptId(), Const.integral.integral_photo);
//        } 
//    }
//    
//    /**
//     * 考试获取积分
//     */ 
//    @Around(value="execution(* com.ifast.exam.service.ExamAnswerQuestionService.integral(..)) && args(type,businessId,isRight)",argNames="type,businessId,isRight")
//    public void versionScore(Const.integral type,String businessId,boolean isRight) throws Throwable { 
//    	UserDO currUser = ShiroUtils.getSysUser();
//        if(null != currUser) {
////        	integralFromService.obtainIntegral(currUser.getId().toString(), currUser.getTopDeptId(),type,businessId,isRight);
//        } 
//    }
}

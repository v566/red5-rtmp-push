package com.ifast.common.utils;

import java.util.Collection;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import cn.hutool.core.collection.CollectionUtil;

public class CheckUtil { 
	 
	public static boolean isNull(Object... params){
    	for(int i=0,end =params.length ; i<end ;i++){ 
    		if(params[i] == null) 
    			return true;
    		if(params[i] instanceof String && StringUtils.isBlank((String) params[i]))
    			return true; 
    		if(params[i] instanceof Map && CollectionUtil.isEmpty((Map<?, ?>) params[i]))
    			return true; 
    		if(params[i] instanceof Collection && CollectionUtil.isEmpty((Collection<?>) params[i]))
    			return true;
    	}
    	return false;
    }
}

package com.ifast.sys.controller;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.google.common.base.Objects;
import com.ifast.common.annotation.Log;
import com.ifast.common.base.AdminBaseController;
import com.ifast.common.domain.DictDO;
import com.ifast.common.domain.Tree;
import com.ifast.common.service.DictService;
import com.ifast.common.utils.MD5Utils;
import com.ifast.common.utils.Result;
import com.ifast.common.utils.ShiroUtils;
import com.ifast.sys.domain.DeptDO;
import com.ifast.sys.domain.RoleDO;
import com.ifast.sys.domain.UserDO;
import com.ifast.sys.service.DeptService;
import com.ifast.sys.service.RoleService;
import com.ifast.sys.service.UserService;
import com.ifast.sys.vo.UserVO;
import com.xiaoleilu.hutool.util.CollectionUtil;
import com.xiaoleilu.hutool.util.StrUtil;

/**
 * <pre>
 * </pre>
 * 
 * <small> 2018年3月23日 | Aron</small>
 */
@RequestMapping("/sys/user")
@Controller
public class UserController extends AdminBaseController {
    private String prefix = "sys/user";

    @Autowired
    UserService userService;

    @Autowired
    RoleService roleService;

    @Autowired
    DictService dictService;
    
    @Autowired
    DeptService deptService;

    @Log("进入系统用户列表页面")
    @RequiresPermissions("sys:user:user")
    @GetMapping("")
    String user(Model model) {
        return prefix + "/user";
    }
    
    @Log("查询系统用户列表")
    @GetMapping("/list")
    @ResponseBody
    public Result<IPage<UserDO>> list(UserDO userDTO) {
        // 查询列表数据
        if(StrUtil.isBlank(userDTO.getDeptId())){
            userDTO.setDeptId(ShiroUtils.getSysUser().getDeptId());
        }
        QueryWrapper<UserDO> wrapper = new QueryWrapper<>();
        wrapper.like(StrUtil.isNotBlank(userDTO.getName()),"name",userDTO.getName());
        if(StrUtil.isNotBlank(userDTO.getDeptId()) && !Objects.equal(userDTO.getDeptId(),"1")){
            wrapper.eq("DEPTID",userDTO.getDeptId());
        }
        wrapper.orderByAsc("create_time");
        IPage<UserDO> page = userService.page(getPage(UserDO.class),wrapper);
        List<UserDO> userDOList = page.getRecords();
        for (UserDO userDO : userDOList) {
            DeptDO deptDO = this.deptService.getById(userDO.getDeptId());
            userDO.setDeptName(deptDO == null ? "" : deptDO.getName());
        }
        return Result.ok(page);
    }

    
    @RequiresPermissions("sys:user:add")
    @Log("添加用户")
    @GetMapping("/add/{deptId}")
    String add(Model model,@PathVariable("deptId") String deptId) {
    	//过滤角色中含有  '超级' 的角色  不要问我为什么！！！！
        List<RoleDO> roles = roleService.list(new QueryWrapper<RoleDO>().notLike("ROLENAME", "超级"));
        model.addAttribute("roles", roles);
        model.addAttribute("deptId", deptId);
        DeptDO deptDO = deptService.getById(deptId);
        model.addAttribute("deptName", deptDO == null ? "" :  deptDO.getName() );
        return prefix + "/add";
    }

    @RequiresPermissions("sys:user:edit")
    @Log("编辑用户")
    @GetMapping("/edit/{id}")
    String edit(Model model, @PathVariable("id") Long id) {
        UserDO userDO = userService.getById(id);
        model.addAttribute("user", userDO);
        List<RoleDO> roles = roleService.findListByUserId(id);
        model.addAttribute("roles", roles);
        return prefix + "/edit";
    }

    @RequiresPermissions("sys:user:add")
    @Log("保存用户")
    @PostMapping("/save")
    @ResponseBody
    Result<String> save(UserDO user) {
        user.setPassword(MD5Utils.encrypt(user.getUsername(), user.getPassword()));
        DeptDO deptDO = new DeptDO();
        deptDO.setId(user.getDeptId());
        List<DeptDO> listUp = this.deptService.listDeptUpTree(deptDO);
        if(CollectionUtil.isNotEmpty(listUp)){
            user.setTopDeptId(listUp.get(0).getId());
        }
        userService.save(user);
        return Result.ok();
    }

    @RequiresPermissions("sys:user:edit")
    @Log("更新用户")
    @PostMapping("/update")
    @ResponseBody
    Result<String> update(UserDO user) {
        userService.updateById(user);
        return Result.ok();
    }

    @RequiresPermissions("sys:user:edit")
    @Log("更新用户")
    @PostMapping("/updatePeronal")
    @ResponseBody
    Result<String> updatePeronal(UserDO user) {
        userService.updatePersonal(user);
        return Result.ok();
    }

    @RequiresPermissions("sys:user:remove")
    @Log("删除用户")
    @PostMapping("/remove")
    @ResponseBody
    Result<String> remove(Long id) {
        userService.removeById(id);
        return Result.ok();
    }

    @RequiresPermissions("sys:user:batchRemove")
    @Log("批量删除用户")
    @PostMapping("/batchRemove")
    @ResponseBody
    Result<String> batchRemove(@RequestParam("ids[]") Long[] userIds) {
        userService.removeByIds(Arrays.asList(userIds));
        return Result.ok();
    }
    
    @Log("退出")
    @PostMapping("/exit")
    @ResponseBody
    boolean exit(@RequestParam Map<String, Object> params) {
        // 存在，不通过，false
        return !userService.exit(params);
    }

    @RequiresPermissions("sys:user:resetPwd")
    @Log("请求更改用户密码")
    @GetMapping("/resetPwd/{id}")
    String resetPwd(@PathVariable("id") Long userId, Model model) {

        UserDO userDO = new UserDO();
        userDO.setId(userId);
        model.addAttribute("user", userDO);
        return prefix + "/reset_pwd";
    }

    @Log("提交更改用户密码")
    @PostMapping("/resetPwd")
    @ResponseBody
    Result<String> resetPwd(UserVO userVO) {
        userService.resetPwd(userVO, getUser());
        return Result.ok();
    }

    @RequiresPermissions("sys:user:resetPwd")
    @Log("admin提交更改用户密码")
    @PostMapping("/adminResetPwd")
    @ResponseBody
    Result<String> adminResetPwd(UserVO userVO) {
        userService.adminResetPwd(userVO);
        return Result.ok();

    }
    
    @Log("查询系统用户属性树形数据")
    @GetMapping("/tree")
    @ResponseBody
    public Tree<DeptDO> tree() {
        Tree<DeptDO> tree = new Tree<DeptDO>();
        tree = userService.getTree();
        return tree;
    }
    
    @Log("进入系统用户树形显示页面")
    @GetMapping("/treeView")
    String treeView() {
        return prefix + "/userTree";
    }
    
    @Log("进入个人中心")
    @GetMapping("/personal")
    String personal(Model model) {
        UserDO userDO = userService.getById(getUserId());
        model.addAttribute("user", userDO);
        List<DictDO> hobbyList = dictService.getHobbyList(userDO);
        model.addAttribute("hobbyList", hobbyList);
        List<DictDO> sexList = dictService.getSexList();
        model.addAttribute("sexList", sexList);
        return prefix + "/personal";
    }
    
    @Log("上传头像")
    @ResponseBody
    @PostMapping("/uploadImg")
    Result<?> uploadImg(@RequestParam("avatar_file") MultipartFile file, String avatar_data, HttpServletRequest request)
            throws Exception {
        Map<String, Object> result = new HashMap<>();
        result = userService.updatePersonalImg(file, avatar_data, getUserId());
        return Result.ok(result);
    }
}

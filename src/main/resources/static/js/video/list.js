var prefix = "/video";
var array = [];
$(function() {
    selectedHtml("groupType", "1161827075006111746", $("#groupTypeValue").val()); 
    layui.use('laypage', function(){
        var laypage = layui.laypage;
        var pageNo = $("#pageNo").val();
        var pageSize = $("#pageSize").val();
        var total = $("#total").val();
        laypage.render({
            elem: 'pageId',
            count: total,
            curr: pageNo,
            limit: pageSize,
            first:"首页",
            prev:"上一页",
            last:"尾页",
            next:"下一页",
            theme:"#1E9FFF",
            jump: function(obj, first){
                if(first != true){//是否首次进入页面
                    var curr = obj.curr;
                    $("#pageNo").val(curr);
                    $("#form").submit();
                }
            }
        });
    });
    layui.use('laydate',function () {
        var laydate = layui.laydate;
        laydate.render({
            elem:"#createTime"
        });
    })
});
 
var search = function () {
    $("#pageNo").val(1);
    $("#pageSize").val(10);
    $("#form").submit();
};

//新增
function add() {
    layui.use('layer', function() {
        var layer = layui.layer;
        layer.open({
            type : 2,
            title : '增加',
            maxmin : true,
            shadeClose : false, // 点击遮罩关闭层
            area : [ '800px', '520px' ],
            success:function (layero,index) {
                layer.full(index);
            },
            content : prefix + '/add'
        });
    });
}

function viewPush(id) {
    layui.use('layer', function() {
        var layer = layui.layer;
        layer.open({
            type : 2,
            title : '查看',
            maxmin : true,
            shadeClose : false, // 点击遮罩关闭层
            area : [ '800px', '520px' ],
            success:function (layero,index) {
                layer.full(index);
            },
            content : prefix + '/viewPush/' + id
        });
    });
}
function viewSrc(id) {
    layui.use('layer', function() {
        var layer = layui.layer;
        layer.open({
            type : 2,
            title : '查看',
            maxmin : true,
            shadeClose : false, // 点击遮罩关闭层
            area : [ '800px', '520px' ],
            success:function (layero,index) {
                layer.full(index);
            },
            content : prefix + '/viewSrc/' + id
        });
    });
}

function viewFace() {
       
    parent.layer.open({
        type : 2,
        title : '人脸识别',
        maxmin : true,
        shadeClose : false, // 点击遮罩关闭层
        moveType:1,
        id: 'LAY_layuipro'  ,//设定一个id，防止重复弹出
        area : [ '800px', '520px' ],
        success:function (layero,index) {
        	parent.layer.full(index);
        },
        content : prefix + '/viewFace'
    });
   
}

function edit(id) {
    layui.use('layer', function() {
        var layer = layui.layer; 
        layer.open({
            type : 2,
            title : '编辑',
            maxmin : true,
            shadeClose : false, // 点击遮罩关闭层
            area : [ '800px', '520px' ],
            success:function (layero,index) {
                layer.full(index);
            },
            content : prefix + '/edit/' + id
        });
    });
}
function push(id) {
    layui.use('layer', function() {
        var layer = layui.layer; 
        $.ajax({
            url : prefix + "/push",
            type : "post",
            data : {
                'id' : id
            },
            success : function(r) {
                if (r.code == 0) {
                	layer.load(0, {
              		  icon: 0,
            		  time: 10000,
            		  shade: [0.1,'#fff'] 
            		}, function(){
            		  //do something
            		});
                	setTimeout(function(){ search();},10000) 
                } else {
                    layer.msg(r.msg);
                }
            }
        });
        
    });
}
function endPush(id) {
    layui.use('layer', function() {
        var layer = layui.layer;  
        $.ajax({
            url : prefix + "/endPush",
            type : "post",
            data : {
                'id' : id
            },
            success : function(r) {
                if (r.code == 0) {
                	layer.load(0, {
                		  icon: 0,
                  		  time: 5000, //2秒关闭（如果不配置，默认是3秒）
                  		  shade: [0.1,'#fff'] 
              		}, function(){
              		  //do something
              		});
                	
                	setTimeout(function(){ search();},5000) 
                   
                } else {
                    layer.msg(r.msg);
                }
            }
        });
         
    });
}
function remove(id) {
    layui.use('layer', function() {
        var layer = layui.layer; 
        layer.confirm('确定要删除选中的记录？', {
            btn : [ '确定', '取消' ]
        }, function(index) {
            $.ajax({
                url : prefix + "/remove",
                type : "post",
                data : {
                    'id' : id
                },
                success : function(r) {
                    if (r.code == 0) {
                    	layer.msg(r.msg);
                        search();
                    } else {
                        layer.msg(r.msg);
                    }
                }
            });
        })
    });
}

/**
 * 同步
 */
function sync(id) {
    layui.use('layer', function() {
        var layer = layui.layer;
        layer.confirm('确定同步选中的记录？', {
            btn : [ '确定', '取消' ]
        }, function(index) {
            $.ajax({
                url : prefix + "/sync",
                type : "get",
                data : {
                    'id' : id
                },
                success : function(r) {
                    if (r.code == 0) {
                        layer.close(index);
                        search();
                    } else {
                        layer.confirm("同步成功",{btn:['确定']},function (index) {
                            search();
                        });
                    }
                }
            });
        });
    });
}

/**
 * 作废
 */
function cancel(id) {
    layui.use('layer', function() {
        var layer = layui.layer;
        layer.confirm('确定作废选中的记录？', {
            btn : [ '确定', '取消' ]
        }, function(index) {
            $.ajax({
                url : prefix + "/cancel",
                type : "get",
                data : {
                    'id' : id
                },
                success : function(r) {
                    if (r.code == 0) {
                        layer.close(index);
                        search();
                    } else {
                        layer.msg(r.msg);
                    }
                }
            });
        });
    });
}


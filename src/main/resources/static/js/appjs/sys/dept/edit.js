$().ready(function() {
	validateRule();
    selectedHtml("qylbId","db2bfbfb5a734712a02dfb6c412a768a",$("#qylbOldId").val());
    selectedHtml("qygmId","db2bb5a734712a02dfb6c412a768a",$("#qygmOldId").val());
    selectedHtml("fxdjId","db2bddbb5a734712a02dfb6c412a768a",$("#fxdjOldId").val());
    selectedHtml("jjlxId","db2bfbbb5a734712a02dfb6c412a768a",$("#jjlxOldId").val());
    selectedHtml("qyjyztId","db2bfbbc5a734712a02dfb6c412a768a",$("#qyjyztOldId").val());
});

$.validator.setDefaults({
	submitHandler : function() {
		update();
	}
});
function update() {
	$.ajax({
		cache : true,
		type : "POST",
		url : "/sys/dept/update",
		data : $('#signupForm').serialize(),// 你的formid
		async : false,
		error : function(request) {
			parent.layer.alert("Connection error");
		},
		success : function(data) {
			if (data.code == 0) {
				parent.layer.msg("操作成功");
				parent.reLoad();
				var index = parent.layer.getFrameIndex(window.name); // 获取窗口索引
				parent.layer.close(index);

			} else {
				parent.layer.alert(data.msg)
			}

		}
	});

}
function validateRule() {
    var icon = "<i class='fa fa-times-circle'></i> ";
    $("#signupForm").validate({
        rules : {
            name : {
                required : true
            },
            zyfzr : {
                required : true
            },
            scjydz : {
                required : true
            },
            zjdyq : {
                required : true
            },
            zyjgbm : {
                required : true
            },
            qylbId : {
                required : true
            }
        },
        messages : {
            name : {
                required : icon + "请输入部门名称"
            },
            zyfzr : {
                required : icon + "请输入主要负责人"
            },
            scjydz : {
                required : icon + "请输入生产经营地址"
            },
            zjdyq : {
                required : icon + "请输入镇街道园区"
            },
            zyjgbm : {
                required : icon + "请输入专业监管部门"
            },
            qylbId : {
                required : icon + "请输入行业类别"
            }
        }
    })
}

//字典
var selectedHtml = function(selctId,gValue,hxId){
    $("#"+selctId).empty();
    $.ajax({
        cache : true,
        type : "GET",
        url : "/common/sysDict/listByGroup",
        data : {"gValue":gValue},
        async : false,
        error : function(request) {
            alertMsg("通信出错!");
        },
        success : function(data) {
            if (data.code == 0) {
                var arr = data.data;
                var option = "<option value=''>-请选择-</option>";
                if(hxId == ""){
                    option = "<option value='' selected='selected'>-请选择-</option>";
                }else{
                    option = "<option value='' >-请选择-</option>";
                }
                $("#"+selctId).append(option);
                $.each(arr,function(index,obj){
                    if(obj.value != null){
                        var htmlRes = "";
                        if(obj.id == hxId){
                            htmlRes = "<option value='"+obj.id+"' selected='selected'>"+obj.name+"</option>";
                        }else{
                            htmlRes = "<option value='"+obj.id+"'>"+obj.name+"</option>";
                        }
                        $("#"+selctId).append(htmlRes);
                    }
                });
            } else {
                alertMsg(data.msg)
            }
        }
    });
};
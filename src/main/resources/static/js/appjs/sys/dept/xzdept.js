
var prefix = "/sys/dept"
$(function() {
	load();
    selectedHtml("qylbId","db2bfbfb5a734712a02dfb6c412a768a","");
});

function load() {
    $('#exampleTable')
        .bootstrapTable(
            {
                method : 'get', // 服务器数据的请求方式 get or post
                url : prefix + "/list", // 服务器数据的加载地址
                showRefresh : true,
                iconSize : 'outline',
                toolbar : '#exampleToolbar',
                striped : true, // 设置为true会有隔行变色效果
                dataType : "json", // 服务器返回的数据类型
                pagination : true, // 设置为true会在底部显示分页条
                singleSelect : false, // 设置为true将禁止多选
                // //发送到服务器的数据编码类型
                pageSize : 10, // 如果设置了分页，每页数据条数
                pageNumber : 1, // 如果设置了分布，首页页码
                // search : true, // 是否显示搜索框
                showColumns : false, // 是否显示内容下拉框（选择显示的列）
                sidePagination : "server", // 设置在哪里进行分页，可选值为"client" 或者
                queryParamsType : "",
                // //设置为limit则会发送符合RESTFull格式的参数
                queryParams : function(params) {
                    return {
                        // 说明：传入后台的参数包括offset开始索引，limit步长，sort排序列，order：desc或者,以及所有列的键值对
                        pageNumber : params.pageNumber,
                        pageSize : params.pageSize,
                        name : $('#name').val(),
                        qylbId : $('#qylbId').val()
                    };
                },
                // 返回false将会终止请求
                responseHandler : function(res){
                    console.log(res);
                    return {
                        "total": res.data.total,//总数
                        "rows": res.data.records   //数据
                    };
                },
                columns : [
                    {
                        title : '编号',
                        field : 'id',
                        align : 'center',
                        valign : 'middle',
                        width : '50px'
                    },
                    {
                        field : 'name',
                        title : '部门名称'
                    },
                    {
                        field : 'qylbName',
                        title : '行业类别'
                    },
                    {
                        field : 'zyfzr',
                        title : '主要负责人'
                    },
                    {
                        field : 'zyjgbm',
                        title : '专业监管部门'
                    },
                    {
                        title : '操作',
                        field : 'id',
                        align : 'center',
                        formatter : function(item, row,index) {
                            var e = '<a class="btn btn-primary btn-sm ' + s_edit_h + '" href="#" mce_href="#" title="选择" onclick="xz(\''
                                + row.id+ '\',\''+row.name+'\')"><i class="fa fa-edit"></i></a> ';
                            return e ;
                        }
                    } ]
            });

}

function reLoad() {
    $('#exampleTable').bootstrapTable('refresh');
}

var xz = function (id,name) {
  parent.$("#deptId").val(id);
  parent.$("#deptName").val(name);
  var index = parent.layer.getFrameIndex(window.name); // 获取窗口索引
  parent.layer.close(index);
  parent.$('#exampleTable').bootstrapTable('refresh');
};

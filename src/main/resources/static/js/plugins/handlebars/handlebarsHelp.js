//自定义末班
Handlebars.registerHelper('formatNum', function(num) {
    try{
        return parseFloat(num, 10).toFixed(2);
    }catch(e){
        throw new Error('Handlerbars Helper "expression" can not deal with wrong expression:'+num);
    }
});

Handlebars.registerHelper('compare', function(num1,num2,options) {
    try{
        if (num1 >= num2) {
            return options.fn(this);
        }else {
            return options.inverse(this);
        }
    }catch(e){
        throw new Error('Handlerbars Helper "expression" can not deal with wrong expression:');
    }
});

Handlebars.registerHelper('isNull', function(num1,options) {
    try{
        if (num1) {
            return options.fn(this);
        }else {
            return options.inverse(this);
        }
    }catch(e){
        throw new Error('Handlerbars Helper "expression" can not deal with wrong expression:');
    }
});

//判断状态  
Handlebars.registerHelper('if_zt', function(zt,options) {
    try{
        if (zt == "1") { //为强中
            return options.fn(this);
        }else {
            return options.inverse(this);
        }
    }catch(e){
        throw new Error('Handlerbars Helper "expression" can not deal with wrong expression:');
    }
});

Handlebars.registerHelper('bfb', function(zt,options) {
    try{
       return zt * 100;
    }catch(e){
        throw new Error('Handlerbars Helper "expression" can not deal with wrong expression:');
    }
});


Handlebars.registerHelper('flpd', function(zt,options) {
    try{
    	if (zt == "1") { 
    		return "一元";
        }else {
        	return "十元";
        }
    }catch(e){
        throw new Error('Handlerbars Helper "expression" can not deal with wrong expression:');
    }
});



var prefix='/sys/menu';
fn={
    edit:function (id) {
        layui.use('layer', function() {
            var layer = layui.layer;
            layer.open({
                type : 2,
                title : '编辑',
                maxmin : true,
                shadeClose : false, // 点击遮罩关闭层
                area : [ '800px', '605px' ],
                content : prefix + '/editMenu/'+id
            });
        });
    },
    delete:function (id) {
        layui.use('layer', function() {
            var layer = layui.layer;
            layer.confirm('确认要删除该菜单及所有子菜单吗？', {
                btn : [ '确定', '取消' ]
            }, function(index) {
                $.ajax({
                    url : prefix + "/remove",
                    type : "post",
                    data : {
                        'id' : id
                    },
                    success : function(r) {
                        if (r.code == 0) {
                            layer.close(index);
                            search();
                        } else {
                            layer.msg(r.msg);
                        }
                    }
                });
            })
        });
    },
    add:function (id) {
        layui.use('layer', function() {
            var layer = layui.layer;
            layer.open({
                type : 2,
                title : '增加1',
                maxmin : true,
                shadeClose : false, // 点击遮罩关闭层
                area : [ '800px', '605px' ],
                content : prefix + '/addMenu/'+id
            });
        });
    }

}

var search = function () {
    $("#form").submit();
};
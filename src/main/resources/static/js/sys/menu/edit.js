$().ready(function() {
	validateRule();
});

$.validator.setDefaults({
    submitHandler : function() {
        update();
    }
});
function update() {
    $.ajax({
        cache : true,
        type : "POST",
        url : "/sys/menu/update",
        data : $('#signupForm').serialize(),
        async : false,
        error : function(request) {
            parent.layer.alert("Connection error");
        },
        success : function(data) {
            if (data.code == 0) {
                var index = parent.layer.getFrameIndex(window.name); // 获取窗口索引
                parent.layer.close(index);
                parent.search();
            } else {
                alertMsgTop(data.msg);
            }

        }
    });

}

function validateRule() {
	var icon = "<i class='fa fa-times-circle'></i> ";
	$("#signupForm").validate({
		rules : {
            menuName : {
                required : true
            }
		},
		messages : {
            menuName : {
                required : icon + "请输入菜单名称"
            }
		}
	})
}
var selectHtml = function(selctId,gValue){
    $.ajax({
        cache : true,
        type : "GET",
        url : "/common/sysDict/listByGroup",
        data : {"gValue":gValue},
        async : false,
        error : function(request) {
            alertMsg("通信出错!");
        },
        success : function(data) {
            if (data.code == 0) {
                var arr = data.data;
                var option = "<option value=''>-请选择-</option>";
                $("#"+selctId).append(option);
                $.each(arr,function(index,obj){
                    if(obj.value != null){
                        var htmlRes = "<option value='"+obj.value+"'>"+obj.name+"</option>"
                        $("#"+selctId).append(htmlRes);
                    }
                });
            } else {
                alertMsg(data.msg)
            }
        }
    });
};
//回显
var selectedHtml = function(selctId,gValue,hxId){
    $.ajax({
        cache : true,
        type : "GET",
        url : "/common/sysDict/listByGroup",
        data : {"gValue":gValue},
        async : false,
        error : function(request) {
            alertMsg("通信出错!");
        },
        success : function(data) {
            if (data.code == 0) {
                var arr = data.data;
                var option = "<option value=''>-请选择-</option>";
                if(hxId == ""){
                    option = "<option value='' selected='selected'>-请选择-</option>";
                }else{
                    option = "<option value='' >-请选择-</option>";
                }
                $("#"+selctId).append(option);
                $.each(arr,function(index,obj){
                    if(obj.value != null){
                        var htmlRes = "";
                        if(obj.value == hxId){
                            htmlRes = "<option value='"+obj.value+"' selected='selected'>"+obj.name+"</option>";
                        }else{
                            htmlRes = "<option value='"+obj.value+"'>"+obj.name+"</option>";
                        }
                        $("#"+selctId).append(htmlRes);
                    }
                });
            } else {
                alertMsg(data.msg)
            }
        }
    });
};
var selectedGetOptionName= function(selctId,gValue,hxId){
    $.ajax({
        cache : true,
        type : "GET",
        url : "/common/sysDict/listByGroup",
        data : {"gValue":gValue},
        async : false,
        error : function(request) {
            alertMsg("通信出错!");
        },
        success : function(data) {
            var htmlRes="";
            if (data.code == 0) {
                var arr = data.data;
                $.each(arr,function(index,obj){
                    if(obj.value != null&& obj.id == hxId){
                        htmlRes = obj.name;
                    }
                });
            } else {
                alertMsg(data.msg)
            }
            $("#"+selctId).append(htmlRes);
        }
    });
}
var selectedHtmlCheck = function(selctId,gValue,hxId){
    var hxvalue = $("#"+hxId).val();
    var hxArr = hxvalue==""?[]:hxvalue.split(",");

    $.ajax({
        cache : true,
        type : "GET",
        url : "/common/sysDict/listByGroup",
        data : {"gValue":gValue},
        async : false,
        error : function(request) {
            alertMsg("通信出错!");
        },
        success : function(data) {
            if (data.code == 0) {
                var arr = data.data;
                var option = "<option value=''>-请选择-</option>";
                // if(hxArr .size()==0){
                //     option = "<option value='' selected='selected'>-请选择-</option>";
                // }else{
                //     option = "<option value='' >-请选择-</option>";
                // }
                if (arr.size===0) {
                    option = "<option value='' selected='selected'>-请选择-</option>";
                    $("#"+selctId).append(option);
                }
                $.each(arr,function(index,obj){
                    if(obj.value != null){
                        var htmlRes = "";
                        if(hxArr.indexOf(obj.id)>-1){
                            htmlRes = "<option value='"+obj.id+"' selected='selected'>"+obj.name+"</option>";
                        }else{
                            htmlRes = "<option value='"+obj.id+"'>"+obj.name+"</option>";
                        }
                        $("#"+selctId).append(htmlRes);
                    }
                });
            } else {
                alertMsg(data.msg)
            }
        }
    });
};

//自动控制div高度
var kongzhiHeight = function(divElementId){
    $("#contentIframe", parent.document).height($("#"+divElementId).height());
    $("#externalIframe", parent.parent.document).height($("#"+divElementId).height()+100);
};

//自动控制div高度
var kongzhiHeightMore = function(divElementId,divElementId1,divElementId2){
    $("#contentIframe", parent.document).css("height",$("#"+divElementId).height()+$("#"+divElementId1).height()+$("#"+divElementId2).height());
    $("#externalIframe", parent.parent.document).height($("#"+divElementId).height()+$("#"+divElementId1).height()+$("#"+divElementId2).height()+100);
};

$(function () {
    try {
        var contextPath = $('#contextRootPath',parent.parent.document).html();//获取应用的根目录，我的绝对路径是http://localhost:80/
        parent.parent.global.contextPath = contextPath;
        console.log("项目路径----"+ parent.parent.global.contextPath);
    }catch (e) {
    }
});

function guid() {
    var s = [];
    var hexDigits = "0123456789abcdef";
    for (var i = 0; i < 36; i++) {
        s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
    }
    s[14] = "4";  // bits 12-15 of the time_hi_and_version field to 0010
    s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1);  // bits 6-7 of the clock_seq_hi_and_reserved to 01
    s[8] = s[13] = s[18] = s[23] = "-";

    var uuid = s.join("");
    return uuid;
}

function loading() {
    var  index = layer.msg('正在导出，请稍等', {
        icon: 16
        ,shade: 0.01
        ,time:10*60*1000
        ,offset: '100px'
    });
    var interval  =setInterval(function(){
        if(getCookie("downloadState")=="success"){
            delCookie("downloadState");
            layer.close(index);
            clearInterval(interval);
        }
    },1000);     //setInterval(fn,i) 定时器，每隔i秒执行fn
}



//读取cookies
function getCookie(name)
{
    var arr,reg=new RegExp("(^| )"+name+"=([^;]*)(;|$)");
    if(arr=document.cookie.match(reg))
        return unescape(arr[2]);
    else
        return null;
}

//删除cookies
function delCookie(name)
{
    var exp = new Date();
    exp.setTime(exp.getTime() - 1);
    var cval=getCookie(name);
    if(cval!=null)
        document.cookie= name + "="+cval+";expires="+exp.toGMTString();
}

function setCookie(name,value)
{
    var Days = 30;
    var exp = new Date();
    exp.setTime(exp.getTime() + Days*24*60*60*1000);
    document.cookie = name + "="+ escape (value) + ";expires=" + exp.toGMTString();
}

function setCookieExpTime(name,value,time)
{
    var strsec = getsec(time);
    var exp = new Date();
    exp.setTime(exp.getTime() + strsec*1);
    document.cookie = name + "="+ escape (value) + ";expires=" + exp.toGMTString();
}
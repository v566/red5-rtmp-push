var prefix = "/lucky/rmgl/";

$(function() {
    //左侧单位树
    jsTreeGbxxList();
    selectPageNext();
    $("#dynamic-table input[type=checkbox]").on('click',function(){
        var in_checked = this;
        $(this).closest('table').find('tbody > tr input[type=checkbox]').each(function(){
            if(this != in_checked) $(this).prop('checked', false);
        });
    });
});

//分页
var fyPage = function(){
    layui.use('laypage', function(){
        var laypage = layui.laypage;
        var pageNo = $("#pageNo").val();
        var pageSize = $("#pageSize").val();
        var total = $("#total").val();
        laypage.render({
            elem: 'pageId',
            count: total,
            curr: pageNo,
            limit: pageSize,
            layout: ['count', 'prev', 'page', 'next', 'limit', 'refresh', 'skip'],
            first:"首页",
            prev:"上一页",
            last:"尾页",
            next:"下一页",
            theme:"#1E9FFF",
            jump: function(obj, first){
                if(first != true){//是否首次进入页面
                    var curr = obj.curr;
                    $("#pageNo").val(curr);
                    $("#pageSize").val(obj.limit);
                    selectPageNext();
                }
            }
        });
    });
};

//查询下一页
var selectPageNext = function () {
    $.ajax({
        cache : true,
        type : "GET",
        url : "/sys/user/listGbxxChild.do",
        data : $('#form').serialize(),
        dataType:"html",
        async : false,
        error : function(request) {
            alertMsgTop("网络超时");
        },
        success : function(data) {
            $("#rightPage").html("");
            $("#rightPage").append(data);
            fyPage();
        }
    });
};

//查询干部信息
var selectPageCsh = function () {
    $("#pageNo").val(1);
    $("#pageSize").val(10);
    selectPageNext();
};

//查看干部信息
function grxxView(id) {
    location.href="/gbxx/grxxPage.do?gbxxId="+id;
}

//左侧单位树
var jsTreeGbxxList = function(){
    $.ajax({
        type : "GET",
        url : "/bm/tree",
        success : function(tree) {
            $('#jsTreeGbxxList').jstree({
                'core' : {
                    'data' : tree
                },
                "plugins" : [ "search" ]
            });
        }
    });
};

$('#jsTreeGbxxList').on("changed.jstree", function(e, data) {
    if(typeof (data.node)!="undefined"){
        if(data.node.state.mType=="bmType"){
            $("#deptId").val(data.node.id);
            $("#deptTypeId").val("lx");
        }else {
            $("#deptTypeId").val("bm");
            $("#deptId").val(data.node.id);
        }
    }
    selectPageCsh();
});

var to = false;
function sear() {
    var name = $("#name").val();
    $.ajax({
        type : "GET",
        url : "/bm/tree?name="+name,
        success : function(tree) {
            // loadTree(tree);
            $('#jsTreeGbxxList').jstree(true).settings.core.data = tree; // 新数据
            $('#jsTreeGbxxList').jstree(true).refresh(); //刷新树
            if(to){
                clearTimeout(to);
            }
            to = setTimeout(function(){
                $('#jsTreeGbxxList').jstree(true).search($('#name').val()); //开启插件查询后 使用这个方法可模糊查询节点
            },250);
        }
    });

}


//详情
function grxx(id) {
    layer.open({
        type : 2,
        title : '拟任拟免职务',
        shadeClose : false,
        shade: false,
        maxmin: true,
        area: ['60%', '70%'],
        success: function(layero,index){
            layer.full(index);
        },
        content : "/gbxx/grxxPage.do?gbxxId="+id+"&menuId="+$("#menuId").val(),
    });
}

function resetPwd() {
    var gbxxId = $($("input[type=checkbox]:checked")[0]).attr("id");
    var gbName = $($("input[type=checkbox]:checked")[0]).val();
    if(typeof  gbName=="undefined"){
        alertMsgTop("请选择一位领导干部");
        return;
    }
   /* layui.use('layer', function() {
        var layer = layui.layer;
        layer.confirm('确定要重置'+gbName+"的密码？", {
            btn : [ '确定', '取消' ]
        }, function(index) {
            $.ajax({
                url :  "/sys/user/resetPwd.do",
                type : "post",
                data : {
                    'gbxxId' : gbxxId
                },
                success : function(r) {
                    if (r.code == 0) {
                        layer.close(index);
                    } else {
                        layer.msg(r.msg);
                    }
                }
            });
        })
    });*/
    layer.open({
        type : 2,
        title : '重置密码',
        shadeClose : false,
        shade: false,
        maxmin: true,
        area: ['500px', '400px'],
        success: function(layero,index){
            // layer.full(index);
        },
        content : "/sys/user/modifyPageReset?gbxxId="+gbxxId
    });
}


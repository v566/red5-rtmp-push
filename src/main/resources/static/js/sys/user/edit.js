$().ready(function() {
	// validateRule();
});


// $.validator.setDefaults({
//     submitHandler : function() {
//         update();
//     }
// });
function update() {
    if ($("#username").val()===""||typeof $("#username").val() ==="undefined") {
        layer.tips('请输入登录名','#username');
        $("#username").focus();
        return false;
    }
    if ($("#name").val()=="") {
        layer.tips('请输入真实姓名','#name');
        $("#name").focus();
        return false;
    }
    if ($("#passWord").val()=="") {
        layer.tips('请输入密码','#passWord');
        $("#passWord").focus();
        return false;
    }
    $.ajax({
        cache : true,
        type : "POST",
        url : "/sys/user/update",
        data : $('#signupForm').serialize(),
        async : false,
        error : function(request) {
            parent.layer.alert("Connection error");
        },
        success : function(data) {
            if (data.code == 0) {
                var index = parent.layer.getFrameIndex(window.name); // 获取窗口索引
                parent.layer.close(index);
                parent.search();
            } else {
                alertMsgTop(data.msg);
            }

        }
    });

}

function opendanwei(seeDomId,editInputDomId,dwzwDomId,zwzwDomId) {
    argument = arguments;
    layer.open({
        type: 2,
        title: '选择单位',
        maxmin: true,
        shadeClose: false, // 点击遮罩关闭层
        area: ['800px', '520px'],
        content:  '/gbxx/opendanwei.do'
    });
}
function gzdwChange(domId) {
    var deptId = $("#"+domId).val();
    $.ajax({
        url:"/gbxx/getDwzwZwzwList.do",
        cache:false,
        type:"post",
        data:{deptId:deptId},
        success:function (res) {
            if(res.code==0){
                var zwzw = res.data.zwzw;
                var dwzw = res.data.dwzw;
                if(!(typeof(zwzw)=="undefined"))setHtml(zwzw,argument[3]);
                else  $("#"+argument[3]).empty();
                if(!(typeof(dwzw)=="undefined")) setHtml(dwzw,argument[2]);
                else $("#"+argument[2]).empty();
            }else if(res.code==1){
                $("#"+argument[3]).empty();
                $("#"+argument[2]).empty();
            }
        }
    })
}

function setHtml(arr,selectId) {
    $("#"+selectId).empty();
    var option = "<option value=''>-请选择-</option>";
    $("#"+selectId).append(option);
    $.each(arr,function(index,obj){
        var htmlRes = "<option value='"+obj.id+"'>"+obj.name+"</option>";
        $("#"+selectId).append(htmlRes);
    });
}

//排序序号
var getPaixu = function () {
    //判断是否是主要职务的单位变化
    if(argument[1]=="deptId"){
        $.ajax({
            url:"/gbxx/getPaixu.do",
            type:"post",
            data:{deptId:$("#deptId").val()},
            success:function (res) {
                if (res.code==0)
                    $("#orderBy").val(res.data);
                $("#orderTemp").val(res.data);
            }
        });
    }
};
function validateRule() {
	var icon = "<i class='fa fa-times-circle'></i> ";
	$("#signupForm").validate({
        rules : {
            userName : {
                required : true
            },
            name : {
                required : true
            },
            passWord : {
                required : true
            }
        },
        messages : {
            userName : {
                required : icon + "请输入用户名"
            },
            name : {
                required : icon + "请输入真实姓名"
            },
            passWord : {
                required : icon + "请输入密码"
            }
        }
	})
}
package exam;

import java.util.List;

import com.google.common.collect.Lists;
import com.xiaoleilu.hutool.util.CollectionUtil;

 
public class TestCollection {
	public static void main(String[] args) {
		List<String> a = Lists.newArrayList();
		a.add("a");
		a.add("c");
		a.add("b");
		a.add("d");
		List<String> b = Lists.newArrayList();
		b.add("a");
		b.add("b");
		b.add("c");
		b.add("d");
		System.out.println(CollectionUtil.disjunction(a, b));
	}
}
